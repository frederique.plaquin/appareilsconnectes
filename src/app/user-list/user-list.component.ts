import {Component, OnDestroy, OnInit} from '@angular/core';
import { User } from "../models/User.model";
import { Subscription } from "rxjs";
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {

  users!: User[];
  userSubscription!: Subscription;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userSubscription = this.userService.userSubject.subscribe( // dès que l'objet est créé on souscrit au sujet de l'objet
      (users: User[]) => {
        this.users = users; // émet un array users reçus depuis le subject
      }
    );
    this.userService.emitUsers(); // on émet le subject
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

}
