import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from "rxjs"; // pas sûre que cet import soit nécessaire
import { interval } from 'rxjs';//cet import suffit


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{

  secondes!: number;
  counterSubscription!: Subscription;

  constructor() {}

  ngOnInit() {
    const counter = interval(1000); // début observable
    this.counterSubscription = counter.subscribe( // observer?
      (value:number) => {
        this.secondes = value;
      }
    );
  }

  ngOnDestroy() { // évite tous les pbs de comportement infini
    this.counterSubscription.unsubscribe(); //destruction de l'observer
  }
}
