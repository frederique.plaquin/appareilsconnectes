import {Subject} from "rxjs";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {Injectable} from "@angular/core";



@Injectable()
export class AppareilService {

  appareilSubject = new Subject<any[]>();

  private appareils = <any[]>[
    /*{
      id: 1,
      name: 'Machine à laver',
      status: 'éteint'
    },
    {
      id: 2,
      name: 'Télévision',
      status: 'allumé'
    },
    {
      id: 3,
      name: 'Ordinateur',
      status: 'éteint'
    }*/
  ];

  constructor(private httpClient: HttpClient) {
  }

  emitAppareilSubject() {
    this.appareilSubject.next(this.appareils.slice()); // on émet une copie de la liste des appareils
  }

  getAppareilById(id: number) {
    const appareil = this.appareils.find(
      (appareilObject) => {
        return appareilObject.id === id;
      }
    );
    return appareil;
  }

  switchOnAll() {
    for(let appareil of this.appareils) {
      appareil.status = 'allumé';
    }
    this.emitAppareilSubject(); // émet la liste des objets ainsi les composants souscrits à ce subject en verront le changement
  }

  switchOffAll() {
    for(let appareil of this.appareils) {
      appareil.status = 'éteint';
    }
    this.emitAppareilSubject();
  }

  switchOnOne(index: number) {
    this.appareils[index].status = 'allumé';
    this.emitAppareilSubject();
  }

  switchOffOne(index: number) {
    this.appareils[index].status = 'éteint';
    this.emitAppareilSubject();
  }

  addAppareil(name: string, status: string) {
    const appareilObject = {
      id: 0,
      name: '',
      status:''
    }
    appareilObject.name = name;
    appareilObject.status = status;
    appareilObject.id = this.appareils[(this.appareils.length - 1)].id + 1;

    this.appareils.push(appareilObject);
    this.emitAppareilSubject();
  }

  saveAppareilsToServer() {
    this.httpClient
      // si on utilise méthode post les données seront enregistrées à nouveau à chaque fois qu'on clique hors ce n'est pas ce que l on souhaite
      // on utilisera donc put à la place qui écrasera les données dejà existantes sur l'url 'https://http-client-demo-1cd1f-default-rtdb.europe-west1.firebasedatabase.app/appareils.json'
      .put('https://http-client-demo-1cd1f-default-rtdb.europe-west1.firebasedatabase.app/appareils.json', this.appareils) // url backend firebase suivi du [path].json où on souhaite sauvergarder données (ou autre type de données) suivi des données à enregistrer
      .subscribe({
        complete: () => {
          console.log('Enregistrement terminé'); // completeHandler
        },
        error: (error) => {
          console.log('Erreur de sauvegarde ! ' + error) // errorHandler
        }
        // next: () => { ... },     // nextHandler
        // someOtherProperty: 42
      }
    );
  }

  getAppareilsFromServer() {
    this.httpClient
      .get<any[]>('https://http-client-demo-1cd1f-default-rtdb.europe-west1.firebasedatabase.app/appareils.json')
      .subscribe({
        next: (response:any[]) => {
          this.appareils = response; // .get<any> pour ne pas avoir d'erreur
          this.emitAppareilSubject();
        },
        complete: () => {
          console.log('Les données ont bien été chargées.');
        },
        error: (error) => {
          console.log('Erreur de chargement ! ' + error);
        }
      });
    }
}
